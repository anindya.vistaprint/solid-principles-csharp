using System;
using static System.Console;
using System.Collections.Generic;

namespace SolidPrinciplesCsharp
{
    class OpenCloseprinciple
    {
        public static void Exec()
        {
            var apple = new Product("Apple", Color.Green, Size.Small);
            var tree = new Product("Tree", Color.Green, Size.Large);
            var house = new Product("House", Color.Blue, Size.Large);

            Product[] products = { apple, tree, house };

            var pf = new ProductFilter();
            
            WriteLine("Large Products (OLD):");
            foreach(var p in pf.FilterBySize(products, Size.Large))
            {
                WriteLine($"{p.Name} is large");
            }

            // WriteLine("Green Products (OLD):");

            // foreach(var p in pf.FilterByColor(products, Color.Green))
            // {
            //     WriteLine($"{p.Name} is green");
            // }

            var bf = new BetterFilter();

            WriteLine("\n\nGreen Products (NEW):");
            foreach (var p in bf.Filter(products, new SizeSpecification(Size.Large)))
            {
                WriteLine($"{p.Name} is large");
            }

            // WriteLine("\n\nGreen Products (NEW):");

            // foreach (var p in bf.Filter(products, new ColorSpecification(Color.Green)))
            // {
            //     WriteLine($"{p.Name} is green");
            // }

            WriteLine("\n\nLarge Green Products (NEW):");
            foreach (var p in bf.Filter(products, new AddSpecification<Product>(
                new ColorSpecification(Color.Green),
                new SizeSpecification(Size.Large)
            )))
            {
                WriteLine($"{p.Name} is green and large");
            }
        }
    }

    enum Color
    {
        Red, Green, Blue
    }

    enum Size
    {
        Small, Medium, Large, Huge
    }

    class Product
    {
        public string Name { get; set; }
        public Color Color { get; set; }
        public Size Size { get; set; }

        public Product(string name, Color color, Size size)
        {
            Name = name;
            Color = color;
            Size = size;
        }
    }

    class ProductFilter
    {
        public IEnumerable<Product> FilterBySize(IEnumerable<Product> products, Size size)
        {
            foreach (var product in products)
            {
                if (product.Size == size)
                {
                    yield return product;
                }
            }
        }

        // public IEnumerable<Product> FilterByColor(IEnumerable<Product> products, Color color)
        // {
        //     foreach (var product in products)
        //     {
        //         if (product.Color == color)
        //         {
        //             yield return product;
        //         }
        //     }
        // }

        // This violates the open-close principle - 
        // modification is done on a class instead of extending it
        // Create interfaces to implement open-close principle in this scenario
    }

    interface ISpecification<T>
    {
        bool IsSatisfied(T t);
    }

    interface IFilter<T>
    {
        IEnumerable<T> Filter(IEnumerable<T> items, ISpecification<T> spec);
    }

    class SizeSpecification : ISpecification<Product>
    {
        private Size _size;

        public SizeSpecification(Size size)
        {
            _size = size;
        }
        public bool IsSatisfied(Product p)
        {
            return p.Size == _size;
        }
    }

    class ColorSpecification : ISpecification<Product>
    {
        private Color _color;

        public ColorSpecification(Color color)
        {
            _color = color;
        }
        public bool IsSatisfied(Product p)
        {
            return p.Color == _color;
        }
    }

    class BetterFilter : IFilter<Product>
    {
        public IEnumerable<Product> Filter(IEnumerable<Product> items, ISpecification<Product> spec)
        {
            foreach (var item in items)
            {
                if (spec.IsSatisfied(item))
                {
                    yield return item;
                }
            }
        }
    }

    class AddSpecification<T> : ISpecification<T>
    {
        private ISpecification<T> _first, _second;

        public AddSpecification(ISpecification<T> first, ISpecification<T> second)
        {
            _first = first;
            _second = second;
        }
        public bool IsSatisfied(T t)
        {
            return _first.IsSatisfied(t) && _second.IsSatisfied(t);
        }
    }

}
