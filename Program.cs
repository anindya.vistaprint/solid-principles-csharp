﻿using System;

namespace SolidPrinciplesCsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------------
            // - A class should only have one reason to change
            // - Separation of concerns - different classes
            //   handling different, independent tasks/problems
            // --------------------------------------------------
            // SingleResponsibilityPrinciple.Exec();

            
            // --------------------------------------------------
            // - Classes should be open for extension but closed 
            //   for modification
            // --------------------------------------------------
            // OpenCloseprinciple.Exec();


            // --------------------------------------------------
            // - Should be able to substitute a base-type with 
            //   the derived type (or sub-type)
            // --------------------------------------------------
            // LiskovSubstitutionPrinciple.Exec();


            // --------------------------------------------------
            // - If interface has lots of functionalities,
            //   break it down to small separate interfaces
            // - YAGNI: You Ain't Gonna Need It
            // --------------------------------------------------
            // InterfaceSegregationPrinciple.Exec();


            // --------------------------------------------------
            // - High-level modules should not depend on low-level
            //   ones; use abstraction.
            // --------------------------------------------------
            // DependencyinversionPrinciple.Exec();
        }
    }
}
