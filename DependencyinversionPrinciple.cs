using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;

namespace SolidPrinciplesCsharp
{
    class DependencyinversionPrinciple
    {
        public static void Exec()
        {
            var parent = new Person { Name = "John" };
            var child1 = new Person { Name = "Chris" };
            var child2 = new Person { Name = "Mary" };

            var relationships = new Relationships();
            relationships.AddParentAndChild(parent, child1);
            relationships.AddParentAndChild(parent, child2);

            new Research(relationships);
        }

        enum Relationship
        {
            Parent, Child, Sibling
        }

        class Person
        {
            public string Name { get; set; }
            // public DateTime DateOfBirth { get; set; }
        }

        // low-level
        class Relationships // : IRelationshopBrowser
        {
            private List<(Person, Relationship, Person)> relations = new List<(Person, Relationship, Person)>();

            public void AddParentAndChild(Person parent, Person child)
            {
                relations.Add((parent, Relationship.Parent, child));
                relations.Add((child, Relationship.Child, parent));
            }


            // This exposes the low-level data and does not allow the Relationship
            // class to change the storage logic. in short, it tightly couples the
            // Relationship class with consumers of this method
            public List<(Person, Relationship, Person)> Relations => relations;

            // public IEnumerable<Person> FindAllChildrenOf(string name)
            // {
            //     return relations
            //             .Where(r => r.Item1.Name == name && r.Item2 == Relationship.Parent)
            //             .Select(p => p.Item3);
            // }
        }

        // high-level
        class Research
        {
            public Research(Relationships relationships)
            {
                var relations = relationships.Relations;

                WriteLine("Old Way:\n");
                // This one does not allow the Relationship class to change 
                // the implementation of how it stores the relations
                foreach (var r in relations.Where(
                    x => x.Item1.Name == "John" &&
                            x.Item2 == Relationship.Parent
                ))
                {
                    WriteLine($"John has a child called {r.Item3.Name}");
                }

                // In order for relationship class to manage its own storage logic
                // while exposing the relationship data to the outside world,
                // we can use an interface to abstract this out.
            }

            // public Research(IRelationshopBrowser browser)
            // {
            //     WriteLine("Dependency Inversion Way:\n");
            //     foreach (var child in browser.FindAllChildrenOf("John"))
            //     {
            //         WriteLine($"John has a child called {child.Name}");
            //     }
            // }
        }

        interface IRelationshopBrowser
        {
            IEnumerable<Person> FindAllChildrenOf(string name);
        }
    }
}