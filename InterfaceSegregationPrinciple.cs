using System;
using static System.Console;

namespace SolidPrinciplesCsharp
{
    class InterfaceSegregationPrinciple
    {
        public static void Exec()
        {
            
        }

        class Document
        {

        }

        interface IMachine
        {
            void Print(Document d);
            void Scan(Document d);
            void Fax(Document d);
        }

        class MultiFunctionprinter : IMachine
        {
            public void Fax(Document d)
            {
                // Fax
            }

            public void Print(Document d)
            {
                // Print
            }

            public void Scan(Document d)
            {
                // Scan
            }
        }

        class OldFashionPrinter : IMachine
        {
            public void Fax(Document d)
            {
                // throw error or prompt a msg
            }

            public void Print(Document d)
            {
                // Print
            }

            public void Scan(Document d)
            {
                // throw error or prompt a msg
            }

        }

        // To avoid this al-in-one functionality,
        // segregate the functionalities into specific interfaces

        interface IPrinter
        {
            void Print(Document d);
        }

        interface IScanner
        {
            void Scan(Document d);
        }

        class Photocopier : IPrinter, IScanner
        {
            public void Print(Document d)
            {
                // Print
            }

            public void Scan(Document d)
            {
                // Scan
            }
        }

        interface IMultiFunctionDevice : IScanner, IPrinter // other functions as well
        {

        }

        class MultiFunctionMachine : IMultiFunctionDevice
        {
            private IPrinter printer;
            private IScanner scanner;

            public MultiFunctionMachine(IPrinter p, IScanner s)
            {
                printer = p;
                scanner = s;
            }

            public void Print(Document d)
            {
                printer.Print(d);
            }

            public void Scan(Document d)
            {
                scanner.Scan(d);
            }
        }
    }
}