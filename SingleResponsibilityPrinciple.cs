using System;
using static System.Console;
using System.Collections.Generic;
using System.IO;

namespace SolidPrinciplesCsharp
{
    class SingleResponsibilityPrinciple
    {
        
        public static void Exec()
        {
            Journal j = new Journal();
            j.AddEntry("First entry.");
            j.AddEntry("Second entry.");

            WriteLine(j.ToString());

            // Persistance p = new Persistance();
            // p.Save(j, "journal.txt", true);
        }
    }

    class Journal
    {
        private readonly List<string> entries = new List<string>();
        private static int count = 0;

        public int AddEntry(string text)
        {
            entries.Add($"{++count}: {text}");
            return count;
        }

        public void RemoveEntry(int index)
        {
            entries.RemoveAt(index); // Not a good way to remove entries; just for this demo only, not meant for actual work
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, entries);
        }


        // These methods increase the responsibility of the Journal Class.
        // The Journal class should not be burdened with functionalities like persisting the data, etc.
        // Create a Persistance class instead which would deal with these
        
        // public void Save(string filename)
        // {
        //     File.WriteAllText(filename, ToString());
        // }

        // public void Load(Uri uri)
        // {
        //     // load journal
        // }

        // public Journal Load(string filename)
        // {
        //     // return Journal
        // }
    }

    class Persistance
    {
        public void Save(Journal j, string filename, bool overwrite = false)
        {
            if (overwrite || !File.Exists(filename))
            {
                File.WriteAllText(filename, j.ToString());
            }
        }
    }
}
