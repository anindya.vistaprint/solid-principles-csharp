using System;
using static System.Console;

namespace SolidPrinciplesCsharp
{
    class LiskovSubstitutionPrinciple
    {
        static int Area(Rectangle r) => r.Width * r.Height;

        public static void Exec()
        {
            Rectangle r = new Rectangle(2, 3);
            WriteLine($"{r} has an area of {Area(r)}");

            // Square sq = new Square();
            // sq.Width = 5;
            // WriteLine($"{sq} has an area of {Area(sq)}");

            // Because of inheritance, the following should give the expected output - 25
            Rectangle sq = new Square();
            sq.Width = 5;
            WriteLine($"{sq} has an area of {Area(sq)}");
        }
    }

    class Rectangle
    {
        // public int Width { get; set; }
        // public int Height { get; set; }

        public virtual int Width { get; set; }
        public virtual int Height { get; set; }

        public Rectangle()
        {
            
        }

        public Rectangle(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public override string ToString()
        {
            return $"{nameof(Width)}: {Width}, {nameof(Height)}: {Height}";
        }
    }

    class Square : Rectangle
    {
        // public new int Width
        // {
        //     set { base.Width = base.Height = value; }
        // }

        // public new int Height
        // {
        //     set { base.Width = base.Height = value; }
        // }

        public override int Width
        {
            set { base.Width = base.Height = value; }
        }

        public override int Height
        {
            set { base.Width = base.Height = value; }
        }
    }
}