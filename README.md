# Revisiting SOLID Principles

## S - Single Responsibility Principle

    - A class should only have one reason to change
    - Separation of concerns - different classes handling different, independent tasks/problems

## O - Open/Close Principle

    - Classes should be open for extension but closed for modification

## L - Liskov Substitution Principle

    - Should be able to substitute a base-type with the derived type (or sub-type)

## I - Interface Segregation Principle

    - If interface has lots of functionalities, break it down to small separate interfaces
    - YAGNI: You Ain't Gonna Need It
      - Implementing a huge interface means that you might have to handle all the methods even if you do not need some.

## D - Dependency Inversion Principle

    - High-level modules should not depend on low-level ones; use abstraction.
